import snakeImg from '../../assets/images/spriteSnake.png';
import snakeTailsImg from '../../assets/images/spriteSnake.png';
import mouseImg from '../../assets/images/spriteSouris.png';
import wallImg from '../../assets/images/mur.png';
import groundImg from '../../assets/images/sol.png';
import bonusImg from '../../assets/images/mystere.png';

import backgroundIntroImg from '../../assets/images/marsattakStudioGame.png';
import backgroundHomeImg from '../../assets/images/imgAccueil.png';
import backgroundHelpImg from '../../assets/images/fondAides.png';

import buttonPlayImg from '../../assets/images/jouer.png';
import buttonHelpImg from '../../assets/images/aides.png';
import buttonEasyImg from '../../assets/images/facile.png';
import buttonNormalImg from '../../assets/images/normal.png';
import buttonHardImg from '../../assets/images/difficile.png';
import buttonReturnImg from '../../assets/images/retour.png';
import choiceImg from '../../assets/images/choixAccueil.png';

const assets = {
  snakeImg,
  snakeTailsImg,
  mouseImg,
  wallImg,
  groundImg,
  bonusImg,

  backgroundIntroImg,
  backgroundHomeImg,
  backgroundHelpImg,

  buttonPlayImg,
  buttonHelpImg,
  buttonEasyImg,
  buttonNormalImg,
  buttonHardImg,
  buttonReturnImg,
  choiceImg,
};

export default assets;
