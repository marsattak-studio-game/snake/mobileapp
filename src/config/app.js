const STATE = {
  INTRO: 'INTRO',
  BEFORE_HOME: 'BEFORE_HOME',
  HOME: 'HOME',
  CHOICE_OF_LEVEL: 'CHOICE_OF_LEVEL',
  GAME: 'GAME',
  GAME_OVER: 'GAME_OVER',
  HELP: 'HELP',
};

export {
  STATE, // eslint-disable-line
};
