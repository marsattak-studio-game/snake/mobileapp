import ExpoTHREE from 'expo-three';
import cacheAssetsAsync from './cacheAssetsAsync';

class ResourcesManager {
  constructor() {
    this._assets = {};
  }

  async preloadAssetsAsync({ images = {}, fonts = {}, audio = {} }) {
    this._assets = {};
    // load assets in cache
    await cacheAssetsAsync({
      images: Object.values(images),
      fonts: Object.values(fonts),
      audio: Object.values(audio),
    });

    // Create Texture
    const downloads = [[], []];
    Object.entries(images).forEach((entry) => {
      downloads[0].push(entry[0]);
      downloads[1].push(ExpoTHREE.loadAsync(entry[1]));
    });
    const newAssets = await Promise.all(downloads[1]);
    newAssets.forEach((value, key) => {
      this._assets[downloads[0][key]] = value;
    });
    return newAssets;
  }

  getAsset(asset) {
    if (asset && this._assets[asset]) {
      return this._assets[asset];
    }
    console.warn(`Asset "${asset}" not found.`); // eslint-disable-line
    return null;
  }
}

export default ResourcesManager;
