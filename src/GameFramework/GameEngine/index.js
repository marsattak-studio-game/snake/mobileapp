import ExpoTHREE, { THREE } from 'expo-three';

const SCREEN_ORIENTATION = {
  LANDSCAPE: 'landscape',
  PORTRAIT: 'portrait',
};

class GameEngine {
  constructor(options = {}) {
    this.scene = null;
    this.camera = null;
    this.renderer = null;

    this.preload = options.preload ? options.preload : () => {};
    this.create = options.create ? options.create : () => {};
    this.update = options.update ? options.update : () => {};

    this.defaultWidth = options.width || 1280;
    this.defaultHeight = options.height || 768;
    this.screenOrientation = options.screenOrientation || SCREEN_ORIENTATION.LANDSCAPE;

    this.worldSpaceWidth = options.width || 1280;
    this.worldSpaceHeight = options.height || 768;
  }

  async _preload() {
    await this.preload();
  }

  _create(gl) {
    const { drawingBufferWidth: glWidth, drawingBufferHeight: glHeight } = gl;

    this.renderer = new ExpoTHREE.Renderer({ gl });
    this.renderer.setSize(glWidth, glHeight);
    this.renderer.setClearColor(0x00FF00, 1);

    if (this.screenOrientation === SCREEN_ORIENTATION.LANDSCAPE) {
      this.worldSpaceWidth = (glWidth / glHeight) * this.worldSpaceHeight;
    } else if (this.screenOrientation === SCREEN_ORIENTATION.PORTRAIT) {
      this.worldSpaceHeight = (glHeight / glWidth) * this.worldSpaceWidth;
    }

    this.camera = new THREE.OrthographicCamera(
      this.worldSpaceWidth / -2,
      this.worldSpaceWidth / 2,
      this.worldSpaceHeight / 2,
      this.worldSpaceHeight / -2,
      0,
      1,
    );

    this.camera.position.x += (this.worldSpaceWidth / 2) - 32 / 2;
    this.camera.position.y += (this.worldSpaceHeight / 2) - 32 / 2;


    this.scene = new THREE.Scene();
    this.scene.size = {
      width: this.worldSpaceWidth,
      height: this.worldSpaceHeight,
    };
  }

  _update(gl) {
    let lastFrameTime = 0.16666;
    const render = () => {
      const now = 0.001 * global.nativePerformanceNow();
      const dt = now - lastFrameTime;

      requestAnimationFrame(render); // eslint-disable-line
      this.update(dt);
      if (dt > 0.150) {
        this.renderer.render(this.scene, this.camera);

        gl.endFrameEXP();
        lastFrameTime = now;
      }
    };
    render();
  }

  async run(gl) {
    await this._preload();
    this._create(gl);
    await this.create({
      gl,
    });
    this._update(gl);
  }
}

export default GameEngine;
