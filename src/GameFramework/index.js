import GameEngine from './GameEngine';
import ResourcesManager from './ResourcesManager';
import Image from './GameObjects/Image';
import Rect from './GameObjects/Rect';

export default GameEngine;

export {
  ResourcesManager,
  Image,
  Rect,
};
