import { THREE } from 'expo-three';

class Image extends THREE.Mesh {
  constructor(imgSource, width = 32, height = 32) {
    const geometry = new THREE.PlaneGeometry(width, height);
    const material = new THREE.MeshBasicMaterial({
      map: imgSource,
      transparent: true,
    });
    super(geometry, material);
    this._imgSource = imgSource;
    this._defaultWidth = width;
    this._defaultHeight = height;
  }

  setSpriteSheet(xKey, yKey, xSpriteNumber, ySpriteNumber) {
    this.material.map.offset.x = ((1 / xSpriteNumber) * xKey);
    this.material.map.offset.y = ((1 / ySpriteNumber) * yKey);
    this.material.map.repeat.x = 1 / xSpriteNumber;
    this.material.map.repeat.y = 1 / ySpriteNumber;
  }

  clone() {
    const newMesh = new Image(
      this._imgSource,
      this._defaultWidth,
      this._defaultHeight,
    );
    return newMesh;
  }
}

export default Image;
