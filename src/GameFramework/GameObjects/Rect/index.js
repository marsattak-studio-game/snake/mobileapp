import { THREE } from 'expo-three';

class Rect extends THREE.Mesh {
  constructor(width = 32, height = 32) {
    const geometry = new THREE.PlaneGeometry(width, height);
    const material = new THREE.MeshBasicMaterial();
    super(geometry, material);
    this._defaultWidth = width;
    this._defaultHeight = height;
  }

  setColor(red, green, blue) {
    this.material.color.setRGB(red, green, blue);
  }

  clone() {
    const newMesh = new Rect(this._defaultWidth, this._defaultHeight);
    newMesh.material.color = this.material.color.clone();
    return newMesh;
  }
}

export default Rect;
