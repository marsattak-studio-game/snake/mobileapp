import GameEngine, {
  ResourcesManager,
  Image,
  Rect,
} from '../../GameFramework';
import {
  DIFFICULTY,
  groundsList,
  wallsList,
} from '../../config/levels';
import assets from '../../config/assets';
import Snake from '../Snake';
import Point from '../Point';
import Bonus from '../Bonus';
import Mouse from '../Mouse';
import Ground from '../Ground';
import Wall from '../Wall';


class Game {
  constructor() {
    this._rscMgr = new ResourcesManager();
    this._gameEngine = null;
    this._difficulty = null;
    this._scene = null;
    this._player = null;
    this._point = null;
    this._bonus = null;
    this._mouse = null;
    this._groundsList = [];
    this._wallsList = [];
    this._tactilEvent = {
      moveX: 0,
      moveY: 0,
    };
  }

  preload() {
    return this._rscMgr.preloadAssetsAsync({ images: assets });
  }

  run(context) {
    this._gameEngine = new GameEngine({
      width: 1280,
      height: 768,
      screenOrientation: 'landscape',
      create: (arg) => this._create(arg, { difficulty: DIFFICULTY.EASY }),
      update: () => this._update(),
    });
    this._gameEngine.run(context);
  }

  touchesBegan({ nativeEvent }) {
    const { touches } = nativeEvent;
    const { locationX, locationY } = touches[0];
    this._tactilEvent.moveX = locationX;
    this._tactilEvent.moveY = locationY;
  }

  touchesMoved({ nativeEvent }) {
    const { translationX, translationY } = nativeEvent;
    if (translationX < this._tactilEvent.moveX - 20) {
      if (this._player.getBonus() === 'reverse') {
        this._player.move('right');
      } else {
        this._player.move('left');
      }
      this._tactilEvent.moveX = translationX;
      this._tactilEvent.moveY = translationY;
    } else if (translationX > this._tactilEvent.moveX + 20) {
      if (this._player.getBonus() === 'reverse') {
        this._player.move('left');
      } else {
        this._player.move('right');
      }
      this._tactilEvent.moveX = translationX;
      this._tactilEvent.moveY = translationY;
    } else if (translationY < this._tactilEvent.moveY - 20) {
      if (this._player.getBonus() === 'reverse') {
        this._player.move('down');
      } else {
        this._player.move('up');
      }
      this._tactilEvent.moveX = translationX;
      this._tactilEvent.moveY = translationY;
    } else if (translationY > this._tactilEvent.moveY + 20) {
      if (this._player.getBonus() === 'reverse') {
        this._player.move('up');
      } else {
        this._player.move('down');
      }
      this._tactilEvent.moveX = translationX;
      this._tactilEvent.moveY = translationY;
    }
  }

  touchesEnded() {
    this._tactilEvent.moveX = 0;
    this._tactilEvent.moveY = 0;
  }

  _create({ gl }, { difficulty }) {
    const { drawingBufferWidth: glWidth } = gl;
    this._camera = this._gameEngine.camera;
    this._camera.position.x += (glWidth - this._gameEngine.worldSpaceWidth) - (32 / 2);
    this._scene = this._gameEngine.scene;
    this._difficulty = difficulty;

    const groundImg = this._rscMgr.getAsset('groundImg');
    const wallImg = this._rscMgr.getAsset('wallImg');
    const bonusImg = this._rscMgr.getAsset('bonusImg');
    const mouseImg = this._rscMgr.getAsset('mouseImg');
    const snakeImg = this._rscMgr.getAsset('snakeImg');
    const snakeTailsImg = this._rscMgr.getAsset('snakeTailsImg');

    this._groundsList = groundsList.map((grdPos) => {
      const ground = new Ground(new Image(groundImg), grdPos.x, grdPos.y, -1);
      this._scene.add(ground.getMesh());
      return ground;
    });
    this._wallsList = wallsList[difficulty].map((wallPos) => {
      const wall = new Wall(new Image(wallImg), wallPos.x, wallPos.y, -0.9);
      this._scene.add(wall.getMesh());
      return wall;
    });

    this._point = new Point(new Rect(32, 32));
    this._point.setPosZ(-0.8);
    this._scene.add(this._point.getMesh());

    this._bonus = new Bonus(new Image(bonusImg));
    this._point.setPosZ(-0.8);
    this._scene.add(this._bonus.getMesh());

    this._mouse = new Mouse(new Image(mouseImg));
    this._mouse.setPosZ(-0.8);
    this._scene.add(this._mouse.getMesh());

    this._player = new Snake(
      {
        head: new Image(snakeImg, 32, 32),
        tails: new Image(snakeTailsImg, 32, 32),
      },
      'right',
      3 * 32,
      2 * 32,
    );
    this._player.getMesh().forEach((mesh) => this._scene.add(mesh));
  }

  _update() {
    this._phisicalEngine();
    this._player.update();
    this._point.update(this._wallsList);
    this._bonus.update(this._wallsList);
    this._mouse.update(this._wallsList);
  }

  _phisicalEngine() {
    if (this._player.getPos(0).x < 0) {
      this._player.getPos(0).x = this._gameEngine.defaultWidth - 32;
    } else if (this._player.getPos(0).x + 32 > this._gameEngine.defaultWidth) {
      this._player.getPos(0).x = 0;
    } else if (this._player.getPos(0).y < 0) {
      this._player.getPos(0).y = this._gameEngine.defaultHeight - 32;
    } else if (this._player.getPos(0).y + 32 > this._gameEngine.defaultHeight) {
      this._player.getPos(0).y = 0;
    }

    // *************** Collision with him ***************
    for (let i = 1; i < this._player.getPos().length; i += 1) {
      if (
        this._player.getPos(0).x === this._player.getPos(i).x
        && this._player.getPos(0).y === this._player.getPos(i).y
      ) {
        // TODO: Game Over
        console.log('Game Over');
      }
    }

    // *************** Collision with a wall ***************
    for (let i = 0; i < this._wallsList.length; i += 1) {
      // ********** MOUSE *********
      if (this._mouse.isActivate()) {
        switch (this._mouse.getDir()) {
          case 'up':
            if (
              this._mouse.getPosX() === this._wallsList[i].getPosX()
              && this._mouse.getPosY() + 32 === this._wallsList[i].getPosY()
            ) {
              this._mouse.changeDir(this._wallsList);
            }
            break;
          case 'down':
            if (
              this._mouse.getPosX() === this._wallsList[i].getPosX()
              && this._mouse.getPosY() - 32 === this._wallsList[i].getPosY()
            ) {
              this._mouse.changeDir(this._wallsList);
            }
            break;
          case 'right':
            if (
              this._mouse.getPosX() + 32 === this._wallsList[i].getPosX()
              && this._mouse.getPosY() === this._wallsList[i].getPosY()
            ) {
              this._mouse.changeDir(this._wallsList);
            }
            break;
          case 'left':
            if (
              this._mouse.getPosX() - 32 === this._wallsList[i].getPosX()
              && this._mouse.getPosY() === this._wallsList[i].getPosY()
            ) {
              this._mouse.changeDir(this._wallsList);
            }
            break;
          default:
        }
      }

      // ********** SNAKE *********
      if (
        this._player.getPos(0).x === this._wallsList[i].getPosX()
        && this._player.getPos(0).y === this._wallsList[i].getPosY()
      ) {
        // TODO: Game Over
        console.log('Game Over');
      }
    }


    // *************** Collision with mouse ***************
    if (
      this._mouse.isActivate() && this._player.getPos(0).x === this._mouse.getPosX()
      && this._player.getPos(0).y === this._mouse.getPosY()
    ) {
      this._mouse.setTime(this._mouse.getTime() - 20000);
      this._player.addMouse();
    }

    // *************** Collision with a point ***************
    if (
      this._point.isActivate() && this._player.getPos(0).x === this._point.getPosX()
      && this._player.getPos(0).y === this._point.getPosY()
    ) {
      this._point.setTime(this._point.getTime() - 20000);
      this._player.addPoints(this._point.getColor());
      // app.getMenu().activeBonus();
    }

    // *************** Collision with a bonus ***************
    if (
      this._bonus.isActivate() && this._player.getPos(0).x === this._bonus.getPosX()
      && this._player.getPos(0).y === this._bonus.getPosY()
    ) {
      this._player.addBonus(this._bonus.getType());
      this._bonus.setTime(this._bonus.getTime() - 10000);
      // app.getMenu().activeBonus(this._bonus.getType());
    }
  }
}

export default Game;
