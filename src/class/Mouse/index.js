class Mouse {
  constructor(imageMesh) {
    this._timeActivate = Date.now();
    this._timeAnim = Date.now();
    this._timeMove = Date.now();
    this._timeChangeDir = Date.now();
    this._nextChangeDir = (Math.floor((5000 - 1000) * Math.random()) + 1000);
    this._direction = 'right';
    this._moveSpeed = 450;
    this._anim = true;
    this._imageMesh = imageMesh;
    this.setActivate(false);
    this._imageMesh.setSpriteSheet(0, 0, 4, 2);
  }

  getMesh() {
    return this._imageMesh;
  }

  getPosX() {
    return this.getMesh().position.x;
  }

  getPosY() {
    return this.getMesh().position.y;
  }

  getPosZ() {
    return this.getMesh().position.z;
  }

  setPosX(newX) {
    this.getMesh().position.x = newX;
  }

  setPosY(newY) {
    this.getMesh().position.y = newY;
  }

  setPosZ(newZ) {
    this.getMesh().position.z = newZ;
  }

  getDir() {
    return this._direction;
  }

  getTime() {
    return this._timeActivate;
  }

  setTime(time) {
    this._timeActivate = time;
  }

  setSpriteSheet(x, y) {
    this.getMesh().setSpriteSheet(x, y, 4, 2);
  }

  isActivate() {
    return this._activate;
  }

  setActivate(activate) {
    this._activate = activate;
    this.getMesh().visible = activate;
  }

  update(objectsList) {
    this._animation();
    this._checkCollision(objectsList);
  }

  _animation() {
    let xSprite = 0;
    let ySprite = 0;
    const cpt = Date.now();

    if (cpt - this._timeAnim > this._moveSpeed) {
      this._anim = !this._anim;
      this._timeAnim = Date.now();
    }
    if (this._anim) {
      ySprite = 0;
    } else {
      ySprite = 1;
    }

    if (this._direction === 'right') {
      xSprite = 0;
    } else if (this._direction === 'left') {
      xSprite = 1;
    } else if (this._direction === 'up') {
      xSprite = 2;
    } else if (this._direction === 'down') {
      xSprite = 3;
    }

    this.setSpriteSheet(xSprite, ySprite);
  }

  _checkCollision(walls) {
    const cpt = Date.now();

    if (this.isActivate() && cpt - this._timeActivate > 10000) {
      this._timeActivate = Date.now();
      this.setActivate(false);
      this.setPosX(0);
      this.setPosY(0);
    } else if (!this.isActivate() && cpt - this._timeActivate > 30000) {
      let isFree = false;
      let newX = 0;
      let newY = 0;

      while (!isFree) {
        newX = (Math.floor((40 - 0) * Math.random()) + 0) * 32;
        newY = (Math.floor((24 - 0) * Math.random()) + 0) * 32;
        isFree = true;
        for (let i = 0; i < walls.length; i += 1) {
          if (walls[i].getPosX() === newX && walls[i].getPosY() === newY) {
            isFree = false;
            i = walls.length;
          }
        }
      }
      this.setPosX(0);
      this.setPosY(0);

      this._timeActivate = Date.now();
      this.setActivate(true);
    }

    if (this.isActivate()) {
      if (cpt - this._timeChangeDir > this._nextChangeDir) {
        this.changeDir(walls);
        this._nextChangeDir = (Math.floor((5000 - 2000) * Math.random()) + 2000);
        this._timeChangeDir = Date.now();
      }
      if (cpt - this._timeMove > this._moveSpeed) {
        if (this._direction === 'up') {
          this.setPosY(this.getPosY() - 32);
        } else if (this._direction === 'down') {
          this.setPosY(this.getPosY() + 32);
        } else if (this._direction === 'left') {
          this.setPosX(this.getPosX() - 32);
        } else if (this._direction === 'right') {
          this.setPosX(this.getPosX() + 32);
        }
        this._timeMove = Date.now();
      }
    }
  }

  changeDir(walls) {
    let isFree = false;
    let newDir = 0;

    while (!isFree) {
      newDir = parseInt(Math.floor((4 - 0) * Math.random()) + 0, 10);
      isFree = true;
      for (let i = 0; i < walls.length; i += 1) {
        if (newDir === 0) {
          if (walls[i].getPosX() === this.getPosX() && walls[i].getPosY() === this.getPosY() - 32) {
            isFree = false;
            i = walls.length;
          }
        } else if (newDir === 1) {
          if (walls[i].getPosX() === this.getPosX() && walls[i].getPosY() === this.getPosY() + 32) {
            isFree = false;
            i = walls.length;
          }
        } else if (newDir === 2) {
          if (walls[i].getPosX() === this.getPosX() - 32 && walls[i].getPosY() === this.getPosY()) {
            isFree = false;
            i = walls.length;
          }
        } else if (newDir === 3) {
          if (walls[i].getPosX() === this.getPosX() + 32 && walls[i].getPosY() === this.getPosY()) {
            isFree = false;
            i = walls.length;
          }
        }
      }
    }

    switch (newDir) {
      case 0:
        this._direction = 'up';
        break;
      case 1:
        this._direction = 'down';
        break;
      case 2:
        this._direction = 'left';
        break;
      case 3:
        this._direction = 'right';
        break;
      default:
    }
  }
}

export default Mouse;
