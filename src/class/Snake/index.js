class Snake {
  constructor(imagesMesh, direction, x, y, z = 0) {
    this._score = 0;
    this._textBonus = '';
    this._imageMeshList = [
      imagesMesh.head,
      imagesMesh.tails,
      imagesMesh.tails.clone(),
    ];

    this._imageMeshList.forEach((obj, key) => {
      switch (direction) {
        case 'right':
          this._imageMeshList[key].position.x = x - (key * 32);
          this._imageMeshList[key].position.y = y;
          this._imageMeshList[key].position.z = z;
          break;
        case 'left':
          this._imageMeshList[key].position.x = x + (key * 32);
          this._imageMeshList[key].position.y = y;
          this._imageMeshList[key].position.z = z;
          break;
        case 'up':
          this._imageMeshList[key].position.x = x;
          this._imageMeshList[key].position.y = y - (key * 32);
          this._imageMeshList[key].position.z = z;
          break;
        case 'bottom':
          this._imageMeshList[key].position.x = x;
          this._imageMeshList[key].position.y = y + (key * 32);
          this._imageMeshList[key].position.z = z;
          break;
        default:
      }
    });
    this._tail = 0;
    this._bonus = 'none';
    this._timeBonus = Date.now();
    this._timePoint = Date.now();
    this._direction = direction;
    this._moveSpeed = 150;
    this._lastDir = direction;
    this._timeAnim = Date.now();
  }

  getMesh(index = null) {
    if (index !== null) {
      if (index >= this._imageMeshList.length) {
        console.log(`Error: The index ${index} does not exist`); // eslint-disable-line no-console
      } else {
        return this._imageMeshList[index];
      }
    }
    return this._imageMeshList;
  }

  getPos(index = 0) {
    if (index >= this.getMesh().length) {
      console.log(`Error: The index ${index} does not exist`); // eslint-disable-line no-console
      return this.getMesh();
    }
    return this.getMesh(index).position;
  }

  getScore() {
    return this._score;
  }

  getTextBonus() {
    return this._textBonus;
  }

  move(newDirection) {
    this._direction = newDirection;
  }

  addPoints(color) {
    if (color === 'blue') {
      this._score += 50;
      this._tail += 1;
      this._textBonus = '+ 50 points';
      this._timePoint = Date.now();
    } else if (color === 'green') {
      this._score += 75;
      this._tail += 2;
      this._textBonus = '+ 75 points';
      this._timePoint = Date.now();
    } else if (color === 'gold') {
      this._score += 100;
      this._tail += 3;
      this._textBonus = '+ 100 points';
      this._timePoint = Date.now();
    }
  }

  addBonus(bonus) {
    if (bonus === 'point') {
      this._score += 200;
      this._timeBonus = Date.now();
      this._textBonus = ' *** + 200points ***';
    } else if (bonus === 'reverse') {
      this._bonus = bonus;
      this._timeBonus = Date.now();
      this._textBonus = ' *** INVERSEMENT ***';
    }
  }

  getBonus() {
    return this._bonus;
  }

  addMouse() {
    if (this.getMesh().length > 3) {
      this._textBonus = ' *** Huuuum une souris ***';
      this.getMesh().splice(this.getMesh().length - 1, this.getMesh().length);
    }
  }

  update() {
    this._update();
    this._animation();
  }

  _animation() {
    let reverse = 1;
    if (this._bonus === 'reverse') {
      reverse = 0;
    }
    let head;
    if (this._lastDir === 'right') {
      head = 1;
    } else if (this._lastDir === 'left') {
      head = 2;
    } else if (this._lastDir === 'down') {
      head = 3;
    } else if (this._lastDir === 'up') {
      head = 4;
    }
    this.getMesh(0).setSpriteSheet(head, reverse, 5, 2);
    for (let i = 1; i < this.getMesh().length; i += 1) {
      this.getMesh(i).setSpriteSheet(0, reverse, 5, 2);
    }
  }

  _update() {
    const tmp = Date.now();
    const tmpBonus = Date.now();
    if (tmpBonus - this._timeBonus > 10000) {
      this._bonus = 'none';
    }
    if (tmpBonus - this._timeBonus > 10000 && tmpBonus - this._timePoint > 10000) {
      this._textBonus = '';
    }
    if (tmp - this._timeAnim > this._moveSpeed) {
      if (this._lastDir === 'left' && this._direction !== 'right') {
        this._lastDir = this._direction;
      } else if (this._lastDir === 'right' && this._direction !== 'left') {
        this._lastDir = this._direction;
      } else if (this._lastDir === 'down' && this._direction !== 'up') {
        this._lastDir = this._direction;
      } else if (this._lastDir === 'up' && this._direction !== 'down') {
        this._lastDir = this._direction;
      }
      if (this._tail > 0) {
        this.getMesh().push(
          this.getMesh(this.getMesh().length - 1).clone(),
        );
        this._tail -= 1;
      }

      for (let i = this.getMesh().length - 1; i >= 0; i -= 1) {
        if (i === 0) {
          if (this._lastDir === 'left') {
            this.getPos(0).x -= 32;
          }
          if (this._lastDir === 'right') {
            this.getPos(0).x += 32;
          }
          if (this._lastDir === 'up') {
            this.getPos(0).y += 32;
          }
          if (this._lastDir === 'down') {
            this.getPos(0).y -= 32;
          }
        } else {
          this.getPos(i).x = this.getPos(i - 1).x;
          this.getPos(i).y = this.getPos(i - 1).y;
        }
      }
      this._timeAnim = Date.now();
    }
  }
}

export default Snake;
