class Ground {
  constructor(imageMesh, x, y, z = 0) {
    this._imageMesh = imageMesh;
    this._imageMesh.position.x = x;
    this._imageMesh.position.y = y;
    this._imageMesh.position.z = z;
  }

  getMesh() {
    return this._imageMesh;
  }

  getPosX() {
    return this._imageMesh.position.x;
  }

  getPosY() {
    return this._imageMesh.position.y;
  }

  setPosX(newX) {
    this.getMesh().position.x = newX;
  }

  setPosY(newY) {
    this.getMesh().position.y = newY;
  }

  setPosZ(newZ) {
    this.getMesh().position.z = newZ;
  }
}

export default Ground;
