class Bonus {
  constructor(imageMesh) {
    this._imageMesh = imageMesh;
    this._type = 'point';
    this.setActivate(false);
    this._timeActivate = Date.now();
  }

  getMesh() {
    return this._imageMesh;
  }

  getPosX() {
    return this.getMesh().position.x;
  }

  getPosY() {
    return this.getMesh().position.y;
  }

  getPosZ() {
    return this.getMesh().position.z;
  }

  setPosX(newX) {
    this.getMesh().position.x = newX;
  }

  setPosY(newY) {
    this.getMesh().position.y = newY;
  }

  setPosZ(newZ) {
    this.getMesh().position.z = newZ;
  }

  getTime() {
    return this._timeActivate;
  }

  setTime(time) {
    this._timeActivate = time;
  }

  getType() {
    return this._type;
  }

  isActivate() {
    return this._activate;
  }

  setActivate(activate) {
    this._activate = activate;
    this.getMesh().visible = activate;
  }

  update(walls) {
    const cpt = Date.now();
    if (this.isActivate() && cpt - this._timeActivate > 10000) {
      this._timeActivate = Date.now();
      this.setActivate(false);
      this._type = 'point';
      this.setPosX(0);
      this.setPosY(0);
    } else if (!this.isActivate() && cpt - this._timeActivate > 20000) {
      let isFree = false;
      let newX = 0;
      let newY = 0;
      while (!isFree) {
        newX = (Math.floor((40 - 0) * Math.random()) + 0) * 32;
        newY = (Math.floor((24 - 0) * Math.random()) + 0) * 32;
        isFree = true;
        for (let i = 0; i < walls.length; i += 1) {
          if (walls[i].getPosX() === newX && walls[i].getPosY() === newY) {
            isFree = false;
            i = walls.length;
          }
        }
      }
      this.setPosX(newX);
      this.setPosY(newY);

      const alt = (Math.floor((100 - 0) * Math.random()) + 0);

      if (alt <= 50) {
        this._type = 'point';
      } else if (alt <= 100) {
        this._type = 'reverse';
      }

      this._timeActivate = Date.now();
      this.setActivate(true);
    }
  }
}

export default Bonus;
