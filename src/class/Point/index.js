class Point {
  constructor(rectMesh) {
    this._rectMesh = rectMesh;
    this.setColor('blue');
    this.setActivate(false);
    this._timeActivate = Date.now();
  }

  getMesh() {
    return this._rectMesh;
  }

  getPosX() {
    return this.getMesh().position.x;
  }

  getPosY() {
    return this.getMesh().position.y;
  }

  getPosZ() {
    return this.getMesh().position.z;
  }

  setPosX(newX) {
    this.getMesh().position.x = newX;
  }

  setPosY(newY) {
    this.getMesh().position.y = newY;
  }

  setPosZ(newZ) {
    this.getMesh().position.z = newZ;
  }

  getColor() {
    return this._color;
  }

  setColor(color) {
    switch (color) {
      case 'blue':
        this.getMesh().setColor(0, 0, 1);
        break;
      case 'green':
        this.getMesh().setColor(0, 0, 1);
        break;
      case 'gold':
        this.getMesh().setColor(0, 0, 1);
        break;
      default:
        this.getMesh().setColor(1, 1, 1);
    }
    this._color = color;
  }

  getTime() {
    return this._timeActivate;
  }

  setTime(time) {
    this._timeActivate = time;
  }

  isActivate() {
    return this._activate;
  }

  setActivate(activate) {
    this._activate = activate;
    this.getMesh().visible = activate;
  }

  update(objectsList) {
    this._checkCollision(objectsList);
  }

  _checkCollision(walls) {
    const cpt = Date.now();
    if (this.isActivate() && cpt - this._timeActivate > 20000) {
      this._timeActivate = Date.now();
      this.setActivate(false);
      this.setColor('blue');
      this.setPosX(0);
      this.setPosY(0);
    } else if (!this.isActivate() && cpt - this._timeActivate > 3000) {
      let isFree = false;
      let newX = 0;
      let newY = 0;
      while (!isFree) {
        newX = (Math.floor((40 - 0) * Math.random()) + 0) * 32;
        newY = (Math.floor((24 - 0) * Math.random()) + 0) * 32;
        isFree = true;
        for (let i = 0; i < walls.length; i += 1) {
          if (walls[i].getPosX() === newX && walls[i].getPosY() === newY) {
            isFree = false;
            i = walls.length;
          }
        }
      }
      this.setPosX(newX);
      this.setPosY(newY);

      const alt = (Math.floor((100 - 0) * Math.random()) + 0);

      if (alt <= 45) {
        this.setColor('blue');
      } else if (alt <= 75) {
        this.setColor('green');
      } else {
        this.setColor('gold');
      }
      this._timeActivate = Date.now();
      this.setActivate(true);
    }
  }
}

export default Point;
