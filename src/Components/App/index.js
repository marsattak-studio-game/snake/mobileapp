import React, { useState } from 'react';
import { GLView } from 'expo-gl';
import { AppLoading } from 'expo';
import { PanGestureHandler } from 'react-native-gesture-handler';

import Game from '../../class/Game';

const game = new Game();

const App = () => {
  const [isLoading, setIsLoading] = useState(true);

  if (isLoading) {
    return (
      <AppLoading
        onFinish={() => setIsLoading(false)}
        startAsync={() => game.preload()}
      />
    );
  }

  return (
    <PanGestureHandler
      onGestureEvent={(e) => game.touchesMoved(e)}
    >
      <GLView
        style={{ flex: 1 }}
        onContextCreate={(context) => game.run(context)}
      />
    </PanGestureHandler>
  );
};

export default App;
